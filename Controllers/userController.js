const User = require ("../models/User");
const Product = require ("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Controller for User Registration

module.exports.registerUser = (userInfo) => {

	let newUser = new User ({

		email : userInfo.email,
		password: bcrypt.hashSync(userInfo.password, 10) 

	});


return newUser.save().then((user, error) => {
	if(error) {
		return false
	}
	else{
		return "Successfully Registered"
	}

})
}

//User Authentication
module.exports.loginUser = (user) => {

	return User.findOne({email: user.email}).then(result => {

		if(result == null) {
			
			return "Please enter a valid email"

		} else {

			const isPasswordCorrect = bcrypt.compareSync(user.password, result.password);
			

			if(isPasswordCorrect) {

				console.log(result)

				return { access: auth.createAccessToken(result)}

			// Passwords do not match	
			} else {

				return "Password Incorrect"
			}
		}

	})
}



//Controller for User Checkout (Non-Admin)

module.exports.order = (payLoad, productId, quantity) => {

	console.log(productId)

	//userResult  = ang user details sa database

	if (payLoad.isAdmin === false) {
	
		return Product.findById(productId).then(productResult =>{
			return User.findById(payLoad.id).then(userResult => {
				
				userResult.orders.push(
				{ products:
					[{
						productId: productId,
						name: productResult.name,
						price : productResult.price,
						quantity : quantity,
					
					}],
						totalAmount : quantity * productResult.price,

					
				})

					return userResult.save().then((userResult, err) =>{
						if (err) 
							{return false}

						else
							{return true}
						})
				})
			})}


		else {
			return false
		}
	}

//Controller for Retrieve User Details
module.exports.getProfile = (user) => {
	console.log(user)

	
	return User.findById(user).then(userDetails => {

		userDetails.password = "";
		return userDetails;

	});	
};
