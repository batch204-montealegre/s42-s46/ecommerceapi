const Product = require ("../models/Product");
const User = require("../models/User");
const Archive = require ("../models/Archive");



//Create Product - Admin Only

module.exports.createProduct = (item) => {

	let newProduct = new Product ({

		name : item.name,
		description : item.description,
		price : item.price
	});

	return newProduct.save().then((products, error) => {

		if(error) {
			return false
		}

		else {
			return "Product Added!"
		}
	})
};

//Retrieving All Active Products
module.exports.showActiveProducts = (isActive) =>{
	return Product.find(isActive).then(products => {
		return products
	})	

	};



//Controller for Retrieving Single Product
module.exports.getProduct = (item) => {


	return Product.findById(item.productId).then(result => {

		console.log(result)
		
		return result
	
	})

}

// Controller for Updating a Product - Admin Only								
module.exports.updateProduct = (reqParams, update) => {

			
			let updatedProduct = {
				name: update.name,
				description: update.description,
				price: update.price
			};

			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

				if(error) 
					{return false} 

				else 
					{return product}
			})
};

// Controller for Archiving a Product - Admin Only	

module.exports.archiveProduct = (data, reqBody) => {

	
		return Product.findById(data.productId).then(result => { 

		if (data.payload === true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {

					if(err) {
					
						return false
					
					}  else {

						let newArchive = new Archive ({

								name : product.name,
								description : product.description,
								price : product.price,
								
							});

							return newArchive.save().then((archives, error) => {

								if(error) {
									return false
								}

								else {
									return Product.deleteOne({ isActive: false }).then((deletedItem, err) =>{

										if (err) {
											return false
												}

										else {
											return "Product Added to Archive!"
											}
										})
								}
							})

						}
			})
			 
		} // end of if (data.payload === true)

		 else {

			return "Please login as admin"
		} 
	})

	}
	

	




 


		
							
						
	