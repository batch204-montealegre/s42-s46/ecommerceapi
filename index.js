const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;
const userRoute = require("./Routes/userRoute");
const productRoute = require ("./Routes/productRoute");

const app = express();



mongoose.connect("mongodb+srv://rmontealegre:admin123@cluster0.cuby1ca.mongodb.net/Capstone2?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());
app.use("/users", userRoute);
app.use("/product", productRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});