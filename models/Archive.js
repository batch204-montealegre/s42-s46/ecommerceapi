const mongoose = require("mongoose");
const archiveSchema = new mongoose.Schema(

{
	name : {
		type : String,
		required : [true, "Name is required"]
	},

	description : {
		type : String
	},

	price : {
		type : Number,
		required : [true, "Price is required"]
	},

	isActive:{
		type: Boolean,
		default: false
	},

	addedOn : {
		type : Date,
		default : new Date()
	}

});

module.exports = mongoose.model("Archive", archiveSchema);