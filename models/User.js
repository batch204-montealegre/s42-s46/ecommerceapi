const mongoose = require ("mongoose");
const userSchema = new mongoose.Schema({

	email:{
			type: String,
			required: [true, "Email is required"]
		},

	password:{
			type: String,
			required: [true, "Password is required"]
		},

	isAdmin:{
		type: Boolean,
		default: false
	},

	orders: [
				{
					products: 
						[{
							productId:
							{	type: String},
							name:
							{	type: String },
							price:
							{	type: Number},
							quantity:
							{	type: Number},
							
						}],
					
					totalAmount:
						{
							type: Number
						},
					purchasedOn:
						{
							type: Date,
							default: new Date()
						}
				}
			]

});

module.exports = mongoose.model("User", userSchema);

