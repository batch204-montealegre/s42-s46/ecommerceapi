const express = require ("express");
const router = express.Router();
const productController = require ("../Controllers/productController");
const auth = require("../auth");
const Product= require ("../models/Product");


//Create Product - Admin Only

router.post("/addItem", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	
});

//	Route for Retrieving All Products
	router.get("/allProducts", auth.verify, (req,res) =>
	{
		productController.showActiveProducts(req.body).then(resultFromController => res.send(resultFromController));
	});


//Route for Retrieving Single Product

router.get("/:productId", (req, res) => {
	// console.log(req.params.productId)
	// console.log(req.params)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});

//Route for Updating a Product	- Admin Only

router.put("/update/:productId", auth.verify, (req, res) => {

	//payload

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	
});

// Route for Archiving a Product - Admin Only


router.put('/archive/:productId', auth.verify, (req, res) => {

	console.log(req.params)

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;