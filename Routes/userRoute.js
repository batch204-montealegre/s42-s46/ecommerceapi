const express = require ("express");
const router = express.Router();
const userController = require ("../Controllers/userController");
const auth = require("../auth");

//Route for User registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
	console.log(req.body);

});

//Route for User Authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for User Checkout (Non-Admin)


router.post("/order" , auth.verify, (req, res) =>
	{
		let payLoad = auth.decode(req.headers.authorization)
		let productId = req.body.id
		let quantity = req.body.quantity
	
		
		userController.order(payLoad, productId, quantity).then(resultFromController => res.send(resultFromController));

	});


//Route for Retrieve User Details

router.get("/viewProfile", auth.verify, (req, res) => {
	

	const userData = auth.decode(req.headers.authorization);

	if(userData.id === req.body.id){
	
	userController.getProfile(userData.id, req.body.id).then(resultFromController => res.send(resultFromController));
}
	else{
		res.send (false)
	}

});




module.exports = router;